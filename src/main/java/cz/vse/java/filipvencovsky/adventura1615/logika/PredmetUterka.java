package cz.vse.java.filipvencovsky.adventura1615.logika;

public class PredmetUterka implements IPredmet {
    private static final String NAZEV = "utěrka";
    private boolean hori = false;

    @Override
    public String getNazev() {
        return NAZEV;
    }

    @Override
    public boolean jeSebratelny() {
        return true;
    }

    @Override
    public boolean hori() {
        return hori;
    }

    @Override
    public void setHori(boolean hori) {
        this.hori = hori;
    }

    @Override
    public String getURL() {
        return "uterka.png";
    }
}
