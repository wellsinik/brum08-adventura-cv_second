package cz.vse.java.filipvencovsky.adventura1615.logika;

import java.util.Set;

public class PrikazOtevri implements IPrikaz {
    private static final String NAZEV = "otevři";
    private Hra hra;
    private Batoh ruce;
    private HerniPlan plan;

    /**
     *  Konstruktor
     * @param hra ,ruce, plan
     */
    public PrikazOtevri(Hra hra, Batoh ruce, HerniPlan plan){
        this.hra = hra;
        this.ruce = ruce;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // chybí druhé slovo
            return "Co mám otevřít? Zadej název předmětu";
        }else{
            //Zkontroluje zda se odemikají dveře a jestli náhodou ještě nehoří
            String predmet = parametry[0];
            String textNavrat = null;
            Boolean jeKonec = false;
            Set<IPredmet> predmetyMistnosti = plan.getAktualniProstor().vratPredmety();
            for(IPredmet predmetHledany : predmetyMistnosti){
                String nazveVeci = predmetHledany.getNazev();
                if(nazveVeci.equals("dveře") && predmetHledany.hori() == false){
                    jeKonec = true;

                }
            }
            if(!jeKonec){
                return "Jdou otevřít pouze dveře, které nehoří!";
            }else{
                konecVseho();
                return jakMocUspesne();
            }
        }
    }
    /**
     *  Ukončí hru
     */
    private void konecVseho(){
        hra.setKonecHry(true);
        hra.konecHry();
    }

    /**
     *  Zjistí úspěšnost hry
     */
    private String jakMocUspesne(){
        Set<IPredmet> predmetyVrukou = ruce.vratObsahRukou();
        int counter = 0;
        String kVypsani = null;
        for(IPredmet predmet : predmetyVrukou){
            String aktualni = predmet.getNazev();
            if(aktualni.equals("dítě") || aktualni.equals("fotky")){
                counter += 1;
            }
        }
        switch (counter){
            case 0:
                kVypsani = "Zachránili jste pouze sebe - splněno na 33%";
                break;
            case 1:
                kVypsani = "Zachránili jste sebe a jednu část života - splněno na 66%";
                break;
            case 2:
                kVypsani = "Blahopřeji! zachránili vše co se dalo - splněno na 100%";
                break;
        }
        return kVypsani;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}

