package cz.vse.java.filipvencovsky.adventura1615.main;

import cz.vse.java.filipvencovsky.adventura1615.logika.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainController implements IPozorovatel{

    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button tlacitko;
    @FXML
    public ImageView hrac;
    @FXML
    public ListView vychody;
    public ListView ruce;


    private Hra hra;

    private Batoh batoh;

    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    private ObservableList<String> seznamVRukou = FXCollections.observableArrayList();

    public MainController(){

        this.hra = new Hra();
        this.batoh = new Batoh();
        souradniceProstoru.put("ložnice", new Point2D(-20,15));
        souradniceProstoru.put("obývák", new Point2D(100 ,15));
        souradniceProstoru.put("kuchyň", new Point2D(230,15));
        souradniceProstoru.put("děcák", new Point2D(-20,-70));
        souradniceProstoru.put("předsíň", new Point2D(100,-70));
        souradniceProstoru.put("komora", new Point2D(230,-70));
    }

    @FXML
    public void initialize(){
        vystup.setEditable(false);
        vystup.appendText(hra.vratUvitani()+"\n");
        hra.getHerniPlan().registrace(this);
        hra.getBatoh().registrace(this);
        update();

    }

    /**
     * Zpracuje vstup
     * @param actionEvent
     */
    public void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");
        vstup.setText("");
        if(hra.konecHry()){
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            tlacitko.setDisable(true);
        }
        update();
    }

    /**
     * Změní polohu hráče a načte aktuální obsah batohu
     */
    @Override
    public void update() {
        vychody.getItems().clear();
        seznamVRukou.clear();

        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        vychody.getItems().addAll(aktualniProstor.getVychody());

       hrac.setX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
       hrac.setY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());


    }

    @Override
    public void updateItem() {
        ObservableList<ImageView> backpackItems = FXCollections.observableArrayList();
        Set<IPredmet> obsahRukou = hra.vratObsahRukou();
        ruce.getItems().clear();
        for(IPredmet p : obsahRukou){
            ImageView iw = new ImageView();
            iw.setImage(new Image(p.getURL()));
            iw.setFitHeight(50);
            iw.setFitWidth(170);
            backpackItems.add(iw);
        }
        ruce.setItems(backpackItems);
    }

    /**
     * Načte východ a pošle příkaz
     * @param mouseEvent
     */
    public void nactiVychod(MouseEvent mouseEvent) {
        Prostor cil = (Prostor) vychody.getSelectionModel().getSelectedItem();
        odesliPrikaz("jdi " + cil);
    }


    /**
     * Odešle příkaz
     * @param prikaz
     */
    private void odesliPrikaz(String prikaz) {

        vystup.appendText(prikaz+"\n");

        String vysledek = hra.zpracujPrikaz(prikaz);

        vystup.appendText(vysledek+"\n\n");
    }

    /**
     * Načte novou hru
     * @param actionEvent
     */
    public void onNewGamePress(ActionEvent actionEvent) {
        vystup.clear();
        hra = new Hra();
        vystup.setText(hra.vratUvitani());
        hra.getHerniPlan().registrace(this);
        update();
    }

    /**
     * Zobrazí nápovědu
     * @param actionEvent
     */
    public void onHelpPress(ActionEvent actionEvent) {
            WebView webView = new WebView();
            webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());
            Stage stage = new Stage();
            stage.setTitle("Nápověda");
            stage.setScene(new Scene(webView, 1300, 650));
            stage.show();
    }

    /**
     * Odebere vybraný předmět z batohu a položí ho do aktuální místnosti
     * @param mouseEvent
     */
    public void onItemClick(MouseEvent mouseEvent) {
        String vybrany = "";
       String vybranyText = mouseEvent.getPickResult().toString();
       Set<IPredmet> predmetyHry = hra.getHerniPlan().getVsechnyPredmety();

       for(IPredmet predmet : predmetyHry){
            if(vybranyText.contains(predmet.getNazev())){
                vybrany = predmet.getNazev();
                break;
            }else continue;
       }
       hra.zpracujPrikaz("polož " + vybrany);
       update();

    }
}
