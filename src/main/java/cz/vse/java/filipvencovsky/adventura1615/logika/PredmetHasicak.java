package cz.vse.java.filipvencovsky.adventura1615.logika;

public class PredmetHasicak implements IPredmet {
    private String nazev = "hasičák";
    private boolean hori = false;

    @Override
    public String getNazev() {
        return nazev;
    }

    @Override
    public boolean jeSebratelny() {
        return true;
    }

    @Override
    public boolean hori() {
        return hori;
    }

    @Override
    public void setHori(boolean hori) {
        this.hori = hori;
    }

    @Override
    public String getURL() {
        return "hasicak.png";
    }
}