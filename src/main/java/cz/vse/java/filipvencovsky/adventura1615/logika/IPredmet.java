package cz.vse.java.filipvencovsky.adventura1615.logika;

public interface IPredmet {
    /**
     *  Metoda vrací název předmětu
     *
     *  @return nazev předmětu
     */
    public String getNazev();

    /**
     *  @return zda je možné předmět sebrat
     */
    public boolean jeSebratelny();

    /**
     *  @return zda předmět hoří
     */
    public boolean hori();

    /**
     * Nastaví hodnotu hoří
     * @param hori
     */
    public void setHori(boolean hori);

    /**
     * Předá image URL uloženou v projektu
     */
    public String getURL();
}
