package cz.vse.java.filipvencovsky.adventura1615.logika;


import cz.vse.java.filipvencovsky.adventura1615.main.IPozorovatel;
import cz.vse.java.filipvencovsky.adventura1615.main.Ipozorovatelny;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 *
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Brůna
 */
public class HerniPlan implements Ipozorovatelny {
    private Set<IPozorovatel> seznamPozorovatelu = new HashSet<>();
    private Prostor aktualniProstor;
    private Set<IPredmet> vsechnyPredmety = new HashSet<>();

    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Do pokojů přidává předměty
     *  Jako výchozí aktuální prostor nastaví ložnici.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor loznice = new Prostor("ložnice", " ve které se máma náhle zbudí");
        Prostor obyvak = new Prostor("obývák", " ze které je vidět na hlavní vchod");
        Prostor kuchyn = new Prostor("kuchyň", " ve které se vaří");
        Prostor decak = new Prostor("děcák", " ve které spí malý Míša");
        Prostor predsin = new Prostor("předsíň", " ze které je možno uniknout ven");
        Prostor komora = new Prostor("komora", " pro ukládání různých věcí");



        // přiřazují se průchody mezi prostory (sousedící prostory)
        loznice.setVychod(obyvak);
        obyvak.setVychod(loznice);
        obyvak.setVychod(kuchyn);
        obyvak.setVychod(predsin);
        kuchyn.setVychod(obyvak);
        decak.setVychod(predsin);
        predsin.setVychod(decak);
        predsin.setVychod(komora);
        predsin.setVychod(obyvak);
        komora.setVychod(predsin);

        PredmetHasicak hasicak = new PredmetHasicak();
        PredmetDvere dvere = new PredmetDvere();
        PredmetStul stul = new PredmetStul();
        PredmetDite dite = new PredmetDite();
        PredmetFotky fotky = new PredmetFotky();
        PredmetUterka uterka = new PredmetUterka();

        //přidání všech předmětů do kolekce vsechnyPredmety
        vsechnyPredmety.add(hasicak);
        vsechnyPredmety.add(dvere);
        vsechnyPredmety.add(stul);
        vsechnyPredmety.add(dite);
        vsechnyPredmety.add(fotky);
        vsechnyPredmety.add(uterka);

        loznice.pridejPredmet(uterka);
        obyvak.pridejPredmet(stul);
        kuchyn.pridejPredmet(hasicak);
        kuchyn.pridejPredmet(uterka);
        predsin.pridejPredmet(dvere);
        decak.pridejPredmet(dite);
        decak.pridejPredmet(stul);
        komora.pridejPredmet(fotky);


        aktualniProstor = loznice;  // hra začíná v ložnici
    }
    /**
     * @return Předměty v prostoru
     */
    public Set<IPredmet> getPredmetyProstoru(){
        return aktualniProstor.vratPredmety();
    }

    /**
     *
     * @return všechny předměty hry
     */
    public  Set<IPredmet> getVsechnyPredmety(){ return vsechnyPredmety; }

    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */


    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {

        aktualniProstor = prostor;
        upozorniPozorovatele();
    }

    @Override
    public void registrace(IPozorovatel pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    @Override
    public void odregistrace(IPozorovatel pozorovatel) {
        seznamPozorovatelu.remove(pozorovatel);
    }

    @Override
    public void upozorniPozorovatele() {
        for(IPozorovatel pozorovatel : seznamPozorovatelu)
        {
            pozorovatel.update();
        }
    }
}
