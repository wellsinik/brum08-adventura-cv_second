package cz.vse.java.filipvencovsky.adventura1615.main;

public interface Ipozorovatelny {
    void registrace(IPozorovatel pozorovatel);

    void odregistrace(IPozorovatel pozorovatel);

    void upozorniPozorovatele();
}
