package cz.vse.java.filipvencovsky.adventura1615.logika;

import java.util.Set;

public class PrikazUhas implements IPrikaz {
    private static final String NAZEV = "uhas";
    private Batoh ruce;
    private HerniPlan plan;

    public PrikazUhas(Batoh ruce, HerniPlan plan){
        this.ruce = ruce;
        this.plan = plan;
    }
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length < 2) {
            // chybí slova
            return "Co mám čím uhasit?";
        }else{
            //Zkontroluje zda je užovatel ve správné místnosti a případně uhasí dveře
            String coUhasit = parametry[0];
            String cimUhasit = parametry[1];
            Set<IPredmet> obsahRukou = ruce.vratObsahRukou();
            Prostor prostor = plan.getAktualniProstor();
            String textNaVraceni = null;
            if(prostor.getNazev().equals("předsíň")){
                if(cimUhasit.equals("hasičák") && maCimHasit()){
                    if(coUhasit.equals("dveře")){
                        Set<IPredmet> predmetyMistnosti = prostor.vratPredmety();
                        for(IPredmet predmet : predmetyMistnosti){
                            String nazevPredmetu = predmet.getNazev();
                            if(nazevPredmetu.equals("dveře")){
                                predmet.setHori(false);
                            }
                        }
                        textNaVraceni = "Dveře byly uhašeny!";
                    }else{
                        textNaVraceni = "Jdou uhasit pouze dveře!";
                    }
                }else{
                    textNaVraceni = "Bez hasícího přístroje půjde těžko hasit!";
                }
            }else{
                textNaVraceni = "Nemá cenu hasit ostatní místnosti, vem dítě, vzpomínky a uteč!";
            }
            return textNaVraceni;
        }
    }

    /**
     *  Kontroluje zda má uživatel u sebe hasící přístroj
     */
    private boolean maCimHasit(){
        Set<IPredmet> predmetyVRuce = ruce.vratObsahRukou();
        Boolean maHasicak = false;
        for(IPredmet predmet : predmetyVRuce){
            String nazev = predmet.getNazev();
            if(nazev.equals("hasičák")){
                maHasicak = true;
            }
        }
        return maHasicak;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
