package cz.vse.java.filipvencovsky.adventura1615.logika;

 

import java.util.ArrayList;
import java.util.Set;

public class PrikazPoloz implements IPrikaz{
    private static final String NAZEV = "polož";
    private Batoh ruce;
    private HerniPlan plan;

    public PrikazPoloz(Batoh rce, HerniPlan plan){
        ruce = rce;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // chybí druhé slovo
            return "Co mám položit? Zadej název předmětu";
        }else{
            String predmet = parametry[0];
            Boolean nalezen = false;
            String textNavrat = null;
            Set<IPredmet> obsahRukou = ruce.vratObsahRukou();
            Prostor prostor = plan.getAktualniProstor();
            System.out.println(obsahRukou.size());
            for(IPredmet predmetHledany : obsahRukou){
                if(predmet.equals(predmetHledany.getNazev())){
                    nalezen = true;
                    if(ruce.odeberZRukou(predmetHledany)){
                        textNavrat = "Předmět byl položen v místnosti";
                        prostor.pridejPredmet(predmetHledany);
                        System.out.println(getAktualniRuce());
                    }else{
                        textNavrat = "Máš prázné ruce";
                    }
                }
            }
            if(!nalezen){
                textNavrat = "Předmět nemáš u sebe";
            }
            return textNavrat;
        }
    }

    /**
     *  @return řetězec věcí v rukou
     */
    private String getAktualniRuce(){
        String retezec = "V rukou neseš: ";
        for(IPredmet predmet: ruce.vratObsahRukou()){
            retezec += " " + predmet.getNazev();
        }
        return retezec;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
