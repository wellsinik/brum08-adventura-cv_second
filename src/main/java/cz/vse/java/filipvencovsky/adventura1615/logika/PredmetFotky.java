package cz.vse.java.filipvencovsky.adventura1615.logika;

public class PredmetFotky implements IPredmet {
    private static final String NAZEV = "fotky";
    private boolean hori = false;

    @Override
    public String getNazev() {
        return NAZEV;
    }

    @Override
    public boolean jeSebratelny() {
        return true;
    }

    @Override
    public boolean hori() {
        return hori;
    }

    @Override
    public void setHori(boolean hori) {
        this.hori = hori;
    }

    @Override
    public String getURL() {
        return "fotky.png";
    }
}

