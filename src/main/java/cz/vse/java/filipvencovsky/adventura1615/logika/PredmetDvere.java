package cz.vse.java.filipvencovsky.adventura1615.logika;

public class PredmetDvere implements IPredmet {
    private static final String NAZEV = "dveře";
    private boolean hori = true;

    @Override
    public String getNazev() {
        return NAZEV;
    }

    @Override
    public boolean jeSebratelny() {
        return false;
    }

    @Override
    public boolean hori() {
        return hori;
    }

    @Override
    public void setHori(boolean hori) {
        this.hori = hori;
    }
    @Override
    public String getURL() {
        return "";
    }

}
