package cz.vse.java.filipvencovsky.adventura1615.logika;

public class PredmetStul implements IPredmet{
    private static final String NAZEV = "stůl";
    private boolean hori = false;

    @Override
    public String getNazev() {
        return NAZEV;
    }

    @Override
    public boolean jeSebratelny() {
        return false;
    }

    @Override
    public boolean hori() {
        return hori;
    }

    @Override
    public void setHori(boolean hori) {
        this.hori = hori;
    }
    @Override
    public String getURL() {
        return "";
    }
}
