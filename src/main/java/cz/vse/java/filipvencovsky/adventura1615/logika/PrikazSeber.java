package cz.vse.java.filipvencovsky.adventura1615.logika;




import java.util.ArrayList;
import java.util.*;

public class PrikazSeber implements IPrikaz {
    private static final String NAZEV = "seber";
    private Batoh ruce;
    private HerniPlan plan;

    public PrikazSeber(Batoh rce, HerniPlan plan){
        ruce = rce;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // chybí druhé slovo
            return "Co mám sebrat? Zadej název předmětu";
        }else{
            //Prozkoumá zda jde předmět sebrat popřípadě ho veme do ruky
            String predmet = parametry[0];
            Boolean bylNalezen = false;
            String textNaVraceni = null;
            Prostor prostor = plan.getAktualniProstor();
            Set<IPredmet> aktualniPredmetyVMistnosti = prostor.vratPredmety();
            for(IPredmet predmetHledany : aktualniPredmetyVMistnosti){
                String momentalniPredmet = predmetHledany.getNazev();
                if(momentalniPredmet.equals(predmet)){
                    if(ruce.pridejDoRukou(predmetHledany)){
                        prostor.odeberPredmet(predmetHledany);
                        textNaVraceni = "Úspěšně si vzal/a předmět do rukou";
                        System.out.println(getAktualniRuce());
                    }else{
                        textNaVraceni = "Buď máš plné ruce, nebo předmět nelze sebrat";
                    }
                    bylNalezen = true;
                    break;
                }
            }
            if(!bylNalezen){
                textNaVraceni = "Předmět není v místnosti";
            }
            return textNaVraceni;
        }
    }

    /**
     *  @return řetězec věcí v rukou
     */
    private String getAktualniRuce(){
        String retezec = "V rukou neseš: ";
        for(IPredmet predmet: ruce.vratObsahRukou()){
            retezec += " " + predmet.getNazev();
        }
        return retezec;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}

