package cz.vse.java.filipvencovsky.adventura1615.logika;



import cz.vse.java.filipvencovsky.adventura1615.main.IPozorovatel;
import cz.vse.java.filipvencovsky.adventura1615.main.Ipozorovatelny;

import java.util.*;

public class Batoh implements Ipozorovatelny {
    private final int pocetRukou = 2;
    private Set<IPredmet> obsahRukou;
    private Set<IPozorovatel> seznamPozorovatelu = new HashSet<>();

    public Batoh(){
        this.obsahRukou = new HashSet<>();
    }

    public Set<IPredmet> vratObsahRukou(){
        return obsahRukou;
    }


    public boolean pridejDoRukou(IPredmet objekt){
        if(objekt.jeSebratelny()){
            if(obsahRukou.size() == pocetRukou){
                return false;
            }else{
                obsahRukou.add(objekt);
                upozorniPozorovatele();
                return true;
            }
        } else return false;


    }

    public boolean odeberZRukou(IPredmet objekt){
        if(obsahRukou.size() == 0){
            return false;
        }else{
            obsahRukou.remove(objekt);
            upozorniPozorovatele();
            return true;
        }
    }


    @Override
    public void registrace(IPozorovatel pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    @Override
    public void odregistrace(IPozorovatel pozorovatel) {
        seznamPozorovatelu.remove(pozorovatel);
    }

    @Override
    public void upozorniPozorovatele() {
        for(IPozorovatel pozorovatel : seznamPozorovatelu){
            pozorovatel.updateItem();
        }

    }
}



